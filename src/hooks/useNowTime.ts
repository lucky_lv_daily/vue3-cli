import { ref } from "vue";

const nowTime = ref('00:00:00');
const getNowTime = () => {
    const Now = new Date();
    const hour = Now.getHours() < 10 ? '0' + Now.getHours() : Now.getHours();
    const minute = Now.getMinutes() < 10 ? '0' + Now.getMinutes() : Now.getMinutes();
    const second = Now.getSeconds() < 10 ? '0' + Now.getSeconds() : Now.getSeconds();
    nowTime.value = hour + ':' + minute + ':' + second;
    setTimeout(getNowTime, 1000)
}

export { nowTime, getNowTime };
